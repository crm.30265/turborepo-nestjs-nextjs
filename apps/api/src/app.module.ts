import { Module } from '@nestjs/common';

import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';

@Module({
  imports: [AuthModule, UsersModule],
})
export class AppModule {}

// import { Controller, Request, Post, UseGuards } from '@nestjs/common';
// import { LocalAuthGuard } from './auth/local-auth.guard';
// import { AuthService } from './auth/auth.service';

// @Controller()
// export class AppController {
//   constructor(private authService: AuthService) {}

//   // @UseGuards(LocalAuthGuard)
//   // @Post('auth/login')
//   // async login(@Request() req) {
//   //   return this.authService.login(req.user);
//   // }
// }
