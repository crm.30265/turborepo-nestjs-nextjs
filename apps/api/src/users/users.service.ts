import { Injectable } from '@nestjs/common';

import { CreateUserRequestDto } from './dto/create-user-request.dto';
import { UpdateUserRequestDto } from './dto/update-user-request.dto';
import { UserResponseDto } from './dto/user-response.dto';
import { PrismaService } from 'src/prisma.service';

import { Role } from '@prisma/client';
import { PasswordService } from 'src/services/password.service';

@Injectable()
export class UsersService {
  constructor(
    private prisma: PrismaService,
    private readonly passwordService: PasswordService,
  ) {}

  async signUp(user: CreateUserRequestDto): Promise<UserResponseDto> {
    const password = await this.passwordService.hashPassword(user.password);
    return await this.prisma.user.create({
      data: {
        ...user,
        password,
        role: Role.USER,
      },
    });
  }
  async update(
    id: string,
    user: UpdateUserRequestDto,
  ): Promise<UserResponseDto> {
    return await this.prisma.user.update({
      where: {
        id,
      },
      data: {
        ...user,
      },
    });
  }
  async findAll() {
    return await this.prisma.user.findMany();
  }
  async findOne(id: string) {
    return await this.prisma.user.findUnique({
      where: {
        id,
      },
    });
  }
  async delete(id: string) {
    return await this.prisma.user.delete({
      where: {
        id,
      },
    });
  }
}
