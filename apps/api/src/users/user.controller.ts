import {
  // ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiBody,
  ApiTags,
  ApiParam,
  // ApiParam,
} from '@nestjs/swagger';
import {
  Controller,
  Post,
  Get,
  Body,
  Patch,
  Delete,
  Param,
  HttpStatus,
  // Request, UseGuards
} from '@nestjs/common';
// import { LocalAuthGuard } from 'src/auth/local-auth.guard';
import { UsersService } from './users.service';
import { CreateUserRequestDto } from './dto/create-user-request.dto';
import { UpdateUserRequestDto } from './dto/update-user-request.dto';
import { UserResponseDto } from './dto/user-response.dto';
@ApiTags('user')
@Controller('user')
export class UserController {
  constructor(private userService: UsersService) {}

  // @UseGuards(LocalAuthGuard)
  @Get()
  @ApiOperation({
    summary: 'Get all users',
    description: 'Get all users',
  })
  @ApiResponse({
    type: UserResponseDto,
    status: HttpStatus.OK,
    isArray: true,
  })
  async findAll() {
    return this.userService.findAll();
  }

  @Post('')
  @ApiOperation({
    summary: 'Sign up',
    description: 'Create new user',
  })
  @ApiBody({
    type: CreateUserRequestDto,
  })
  @ApiResponse({
    type: UserResponseDto,
    status: HttpStatus.CREATED,
  })
  async signUp(@Body() body: CreateUserRequestDto) {
    return this.userService.signUp(body);
  }

  @Patch('/:id')
  @ApiOperation({
    summary: 'Update user',
    description: 'Update user by id',
  })
  @ApiParam({
    name: 'id',
  })
  @ApiBody({
    type: UpdateUserRequestDto,
  })
  @ApiResponse({
    type: UserResponseDto,
    status: HttpStatus.CREATED,
  })
  async update(@Param('id') id: string, @Body() body: UpdateUserRequestDto) {
    return this.userService.update(id, body);
  }

  @Delete('/:id')
  @ApiOperation({
    summary: 'Delete user',
    description: 'Delete user by id',
  })
  @ApiParam({
    name: 'id',
  })
  @ApiResponse({
    type: UserResponseDto,
    status: HttpStatus.CREATED,
  })
  async delete(@Param('id') id: string) {
    return this.userService.delete(id);
  }
}
