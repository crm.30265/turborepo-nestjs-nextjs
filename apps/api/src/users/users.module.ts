import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { PrismaService } from 'src/prisma.service';
import { UserController } from './user.controller';
import { PasswordService } from 'src/services/password.service';
import { ConfigService } from '@nestjs/config';
@Module({
  providers: [UsersService, PrismaService, PasswordService, ConfigService],
  controllers: [UserController],
  exports: [UsersService],
})
export class UsersModule {}
